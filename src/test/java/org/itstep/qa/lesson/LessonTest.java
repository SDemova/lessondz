package org.itstep.qa.lesson;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class LessonTest {

    private WebDriver driver;

    @BeforeClass
    public void createBrowser() {
        System.setProperty("webdriver.chrome.driver",
                "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void quitBrowser() {
        driver.quit();
    }

    @BeforeMethod
    public void clear() {
        driver.manage().deleteAllCookies();
    }



    @Test
    public void fieldLengthOne() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.id("login"));
        element.sendKeys("1"+Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[3]/div/div/div[3]"));
        Assert.assertEquals(element.getText(), "Логин должен быть от 3 до 32 символов");

            }

    @Test
    public void fieldLengthTwo() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.id("login"));
        element.sendKeys("12345678901234567890abcdefghijkl34"+Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[3]/div/div/div[3]"));
        Assert.assertEquals(element.getText(), "Логин должен быть от 3 до 32 символов");

    }

    @Test
    public void fieldLengthOneOne() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.id("login"));
        element.sendKeys("1"+Keys.ENTER);

        List<WebElement> elementsList = driver
                .findElements(By.cssSelector(".rui-InputStatus-message"));
        WebElement[] elements = elementsList
                .toArray(new WebElement[elementsList.size()]);
        for (WebElement web : elements) {
            if (web.getText().equals("Логин должен быть от 3 до 32 символов")) {
                Assert.assertEquals(web.getText(), "Логин должен быть от 3 до 32 символов");
                System.out.println("Необходимое сообщение об ошибке найдено");
                break;
            }
        }


    }

    @Test
    public void fieldLengthTwoTwo() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.id("login"));
        element.sendKeys("12345678901234567890abcdefghijkl34"+Keys.ENTER);

        List<WebElement> elementsList = driver
                .findElements(By.cssSelector(".rui-InputStatus-message"));
        WebElement[] elements = elementsList
                .toArray(new WebElement[elementsList.size()]);
        for (WebElement web : elements) {
            if (web.getText().equals("Логин должен быть от 3 до 32 символов")) {
                Assert.assertEquals(web.getText(), "Логин должен быть от 3 до 32 символов");
                System.out.println("Необходимое сообщение об ошибке найдено");
                break;
            }
        }


    }

    @Test
    public void fieldsFirstnameLastname() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.name("firstname"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[1]/div/div/div[2]"));
        Assert.assertEquals(element.getText(), "Поле должно быть заполнено");
        element = driver.findElement(By.name("lastname"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[2]/div/div/div[2]"));
        Assert.assertEquals(element.getText(), "Поле должно быть заполнено");
                   }



    @Test
    public void fieldPasswordCSS() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.name("newPassword"));
        element.sendKeys(Keys.ENTER);

        List<WebElement> elementsList = driver
                .findElements(By.cssSelector(".rui-InputStatus-message"));
        WebElement[] elements = elementsList
                .toArray(new WebElement[elementsList.size()]);

        for (WebElement web : elements) {

            if (web.getText().equals("Пароль должен содержать от 8 до 32 символов, " +
                    "включать хотя бы одну заглавную латинскую букву, одну строчную и одну цифру"))
            {
                Assert.assertEquals(web.getText(),
                        "Пароль должен содержать от 8 до 32 символов," +
                                " включать хотя бы одну заглавную латинскую букву, одну строчную и одну цифру");
                System.out.println("Необходимое сообщение об ошибке найдено");
                break;

            }

        }

        }

    @Test
    public void fieldConfirmPasswordCss() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.name("confirmPassword"));
        element.sendKeys("2233"+Keys.ENTER);

        List<WebElement> elementsList = driver
                .findElements(By.cssSelector(".rui-InputStatus-message"));
        WebElement[] elements = elementsList
                .toArray(new WebElement[elementsList.size()]);

        for (WebElement web : elements) {

            if (web.getText().equals("Пароли не совпадают"))
            {
                Assert.assertEquals(web.getText(),
                        "Пароли не совпадают");
                System.out.println("Необходимое сообщение об ошибке найдено");
                break;

            }

        }

    }


    @Test
    public void fieldConfirmPasswordXPath() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.name("confirmPassword"));
        element.sendKeys("2233"+Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[5]/div/div/div[2]"));
        Assert.assertEquals(element.getText(), "Пароли не совпадают");

    }

    @Test
    public void fieldDateOfBirthXPath() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.id("firstname"));
        element.sendKeys(Keys.ENTER);
         element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[6]/div/div/div[2]"));
        Assert.assertEquals(element.getText(), "Укажите дату рождения полностью");

    }

    @Test
    public void fieldSexXPath() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.id("firstname"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/section[7]/div/div/div[2]"));
        Assert.assertEquals(element.getText(), "Выберите пол");

    }

    @Test
    public void fieldTelephoneOneXPath() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.id("firstname"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/form/div/div/div/section[1]/div/div[1]/div[2]"));
        Assert.assertEquals(element.getText(), "Введите телефон");

    }

    @Test
    public void fieldTelephoneTwoCSS() {
        driver.get("https://id.rambler.ru/account/registration?back=https%3A%2F%2Fmail.rambler.ru%2F%3Futm_source%3Dhead%26utm_campaign%3Dself_promo%26utm_medium%3Dtopline%26utm_content%3Dmail&rname=mail");
        WebElement element = driver.findElement(By.id("firstname"));
        element.sendKeys(Keys.ENTER);
        element = driver.findElement(By.id("Rambler::Id::register_user_by_phone.phone"));
        element.sendKeys("2"+Keys.ENTER);
        element.sendKeys(Keys.ENTER);


        try {
            Thread.sleep(10000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }


        List<WebElement> elementsList = driver
                .findElements(By.cssSelector(".rui-InputStatus-message"));
        WebElement[] elements = elementsList
                .toArray(new WebElement[elementsList.size()]);

        for (WebElement web : elements) {

            if (web.getText().equals("Введите корректный номер телефона"))
            {
                Assert.assertEquals(web.getText(),
                        "Введите корректный номер телефона");
                System.out.println("Необходимое сообщение об ошибке найдено");
                break;

            }


        }



    }





    }

